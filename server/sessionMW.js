const session    = require('express-session');
const RedisStore = require('connect-redis')(session);

module.exports =
session
(
    {
        store: new RedisStore({ client : require('./redisFunc.js').rclient }),
        secret: process.env.COOKIE_SECRET,
        resave: false,
        saveUninitialized : true,
        cookie: { maxAge: 1 * 24 * 60 * 60 * 1000 } // 1 days
    }
);
