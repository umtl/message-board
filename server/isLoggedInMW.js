module.exports = (req, res, next) =>
{
    if(!req.session || !req.session.uname)
        return res.status(401).json({ errCode : 999 });
    else
        next();
}