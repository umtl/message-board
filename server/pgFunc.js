const pg      = require('pg-promise')();
const db      = pg(process.env.DATABASE_URL);

async function login_account(uname, upass)
{
    try
    {
        let result = await db.any
        (`SELECT uname FROM mod WHERE uname=$1 and passd=$2`, [uname, upass]);
        if(!result || !result.length) return -2;
        else return uname;
    }
    catch(err)
    {
        console.error('login func', err);
        return -1;
    }
}

async function postlog(id, ip)
{
    try
    {
        await db.none
        (
            `INSERT INTO postlog(id, ip) VALUES($1, $2)`,
            [id, ip]
        );
    }
    catch(err)
    {
        console.error(err);
    }
}

async function reportlog(id, ip)
{
    try
    {
        await db.none
        (
            `INSERT INTO reportlog(id, ip) VALUES($1, $2)`,
            [id, ip]
        );
    }
    catch(err)
    {
        console.error(err);
    }
}

async function isBanned(ip)
{
    try
    {
        let res = await db.any
        (
            `SELECT * FROM ban WHERE ip=$1 AND (until IS NULL OR until > NOW())`,
            [ip]
        );
        if(!res || !res.length) return false;
        else return res[0];
    }
    catch(err)
    {
        console.error(err);
    }
}

async function createPost(content, rootpost, fname, board, ip)
{
    try
    {
        let res = await db.one
        (
            `INSERT INTO post(content, rootpost, fname, board)
            VALUES($1, $2, $3, $4) RETURNING id`,
            [content, rootpost, fname, board]
        );

        try
        {
            if(rootpost) await db.none
            (
                `UPDATE post SET bumped=NOW(), replies = replies + 1 WHERE id=$1`,
                [rootpost]
            );
        }
        catch(err)
        {
            console.error('bump err', err);
        }

        if(ip) postlog(res.id, ip);

        return res.id;
    }
    catch(err)
    {
        console.error(err);
    }
}

async function readPost(id)
{
    try
    {
        let res = await db.one
        (
            `SELECT * FROM post WHERE id=$1`, [id]
        );

        return res;
    }
    catch(err)
    {
        console.error(err);
    }
}

async function deletePost(id, flag)
{
    try
    {
        await db.none
        (
            `UPDATE post SET content=$1, fname=$2, flag=$3 WHERE id=$4`,
            ['deleted', null, flag || 1, id]
        );

        return id;
    }
    catch(err)
    {
        console.error(err);
    }
}

async function latestPost(page)
{
    // TODO: create another version of this where it finds all posts that have
    // replies in them

    // and another for just latest replies

    try
    {
        if(!page) page = 1;
        else page = Number(page);
        let res = await db.any
        (
            `SELECT *,
            SUBSTRING(content, 0, 300) as content
            FROM post as p
            WHERE rootpost IS NULL
            ORDER BY bumped DESC
            OFFSET ${ page == 1 ? 0 : (page-1) * 30 } LIMIT 30`,
        );

        return res;
    }
    catch(err)
    {
        console.error(err);
        return [];
    }
}

async function deleteOldPosts()
{
    try
    {
        let res = await db.any
        (
            `SELECT id, bumped FROM post
            WHERE rootpost IS NULL
            ORDER BY bumped DESC
            OFFSET ${ 9 * 10 } LIMIT 1`,
        );

        if(!res.length) return true;

        // NOT DONE, TODO
        await db.none
        (
            `DELETE FROM post WHERE bumped >= (
                SELECT id, bumped FROM post
                WHERE rootpost IS NULL
                ORDER BY bumped DESC
                OFFSET ${ 9 * 10 } LIMIT 1
            ) CASCADE`,
            [res[0].bumped]
        );

        return true;
    }
    catch(err)
    {
        console.error(err);
        return false;
    }
}

async function pagePosts(id)
{
    try
    {
        // consider making 2 seperate request
        let res = await db.any
        (
            `SELECT * FROM post
            WHERE (id=$1 AND rootpost IS NULL) OR rootpost=$1
            ORDER BY created ASC`,
            [id]
        );
        return res;
    }
    catch(err)
    {
        console.error(err);
        return [];
    }
}

async function createReport(postid, content, ip)
{
    try
    {
        let res = await db.one
        (
            `INSERT INTO report(postid, content) VALUES($1, $2) RETURNING id`,
            [postid, content]
        );

        if(ip) reportlog(res.id, ip);

        return res.id;
    }
    catch(err)
    {
        console.error(err);
    }
}

async function resolveReport(id)
{
    try
    {
        let res = await db.none
        (
            `UPDATE report SET resolved=$1 WHERE id=$2`, [true, id]
        );
        return true;
    }
    catch(err)
    {
        console.error(err);
        return false;
    }
}

async function modLog(theid, uname, reason, actionFlag)
{
    try
    {
        await db.none
        (
            `INSERT INTO reportresolvelog(theid, mod, reason, flag)
            VALUE($1, $2, $3, $4)`,
            [theid, uname, reason, actionFlag]
        );

        return true;
    }
    catch(err)
    {
        console.error(err);
        return false;
    }
}

async function setBan(ip, postid, until, reason)
{
    // TODO: check if already banned and ask mod whether or not they wish to update
    // now add
    try
    {
        await db.tx(async (t) =>
        {
            let res = await t.any('SELECT until FROM ban WHERE ip=$1', [ip]);
            return await t.batch
            ([
                t.none
                (
                    `INSERT INTO modlog(theid, reason) VALUES($1, $2) ON CONFLICT (theid) DO
                    UPDATE SET reason=$2, created=NOW()`,
                    [postid, reason]
                ),
                until ? // if not banned forever 
                    t.none
                    (
                        `INSERT INTO ban(ip, postid, until) VALUES($1, $2, $3) ON CONFLICT (ip) DO
                        UPDATE SET postid=$2, until=$3 + INTERVAL $4, created=NOW() WHERE excluded.ip=$1`,
                        [
                            ip,
                            postid,
                            until,
                            (res && res.until) ? (String(Math.ceil((new Date(res.until) + new Date(until)) / (1000 * 60 * 60 * 24)))) : until + ' days' ||
                            '0 day'
                        ]
                    )
                :
                    t.none
                    (
                        `INSERT INTO ban(ip, postid, until) VALUES($1, $2, $3) ON CONFLICT (ip) DO
                        UPDATE SET postid=$2, until=$3, created=NOW() WHERE excluded.ip=$1`,
                        [
                            ip,
                            postid,
                            null
                        ]
                    )
            ])
        });

        return true;
    }
    catch(err)
    {
        console.error(err);
        return false;
    }
}

async function getbanInfo(ip)
{
    try
    {
        return await db.one(`SELECT postid as post_banned_for, until, reason FROM ban INNER JOIN modlog ON postid=theid WHERE ip=$1 AND (until IS NULL OR NOW() > until)`, ip);
    }
    catch(err)
    {
        console.error(err);
        return {};
    }
}

async function getbanList()
{
    try
    {
        return await db.any(`SELECT postid as post_banned_for, until, reason FROM ban INNER JOIN modlog ON postid=theid WHERE until IS NULL OR NOW() > until`);
    }
    catch(err)
    {
        console.error(err);
        return {};
    }
}

async function expireBan(postid)
{
    try
    {
        await db.none
        (
            `UPDATE ban SET until=NOW() WHERE postid=$1`, [postid]
        );

        return true;
    }
    catch(err)
    {
        console.error(err);
        return false;
    }
}

async function getReport(num)
{
    // TOFIX: reportid and postid the same
    try
    {
        if(!num) num = 100;
        let res = await db.any
        // ignoring the postlog shananagans for now
        (
            `SELECT * FROM report WHERE resolved IS NOT TRUE
            ORDER BY created DESC`
        );
        /*
        (
            `
            SELECT * FROM
                (SELECT * FROM report WHERE resolved IS NOT TRUE) as a
            INNER JOIN
                postlog
            ON
                a.postid = postlog.id
            ORDER BY
                a.created DESC
            LIMIT $1
            `,
            [num]
        );
        // TODO: set a way to get reports even if there is
        // no postlog entry for it
        */

        return res;
    }
    catch(err)
    {
        console.error(err);
        return [];
    }
}

async function getPostIP(postId)
{
    try
    {
        let res = await db.one('SELECT ip FROM postlog WHERE id=$1', postId);
        return res.ip;
    }
    catch(err)
    {
        console.error(err);
        let e = new Error();
        e.errCode = 3; // TODO: handle error in frontend
        throw e;
    }
}

module.exports =
{
    login_account : login_account,
    postlog       : postlog,
    reportlog     : reportlog,
    isBanned      : isBanned,
    createPost    : createPost,
    readPost      : readPost,
    deletePost    : deletePost,
    latestPost    : latestPost,
    pagePosts     : pagePosts,
    createReport  : createReport,
    resolveReport : resolveReport,
    getReport     : getReport,
    modLog        : modLog,
    setBan        : setBan,
    getbanInfo    : getbanInfo,
    getbanList    : getbanList,
    expireBan     : expireBan,
    getPostIP     : getPostIP,
    db            : db
}