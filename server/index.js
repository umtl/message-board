const express = require('express');
const app     = express();
var bp        = require('body-parser');

app.disable('x-powered-by');

app.use(express.static('public'));

app.use(bp.urlencoded({ extended: true }));
app.use(express.json());

app.use(require('./sessionMW.js'));
app.use(require('./routes/'));

const server = app.listen(process.env.PORT || '8001', (err) =>
{
    if(err) console.error(err);
    else    console.info(server.address());
});