const f = require('./pgFunc.js');

module.exports = async (req, res, next) =>
{
    try
    {
        let isBanned = await f.isBanned((req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim());
        if(isBanned) return res.status(422).json({ errCode : 888, until : null });
        return next();
    }
    catch(err)
    {
        console.error(err);
        return res.status(500).json({ errCode : 881 });
    }

}