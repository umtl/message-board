const svgc = require('svg-captcha');

svgc.options.size = 4;
svgc.options.charPreset = '0123456789';
svgc.options.height = 42;
svgc.options.color = false;
// svgc.options.background = '#292934';

function set_captcha_get_svg(req, force_change)
{
    if(req.session.d && !force_change) return req.session.d;
    let captcha = svgc.create();
    req.session.s = captcha.text; // solution
    req.session.d = captcha.data; // svg data
    return captcha.data;
}

function captcha_is_correct(req)
{
    if(req.body && req.body.solution && req.body.solution === req.session.s)
    {
        delete req.session.s;
        delete req.session.d;
        return true;
    }

    return false;
}


module.exports =
{
    set_captcha_get_svg : set_captcha_get_svg,
    captcha_is_correct  : captcha_is_correct
}