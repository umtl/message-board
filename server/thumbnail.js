const childp  = require('child_process');

module.exports = (input, output, isRootPost, isGif) =>
{
    return new Promise((resolve, reject) =>
    {
        try
        {
            childp.exec
            (
                `convert ${input}${isGif ? '[0]' : ''} -background "#e9e9e9" -alpha remove -thumbnail '${isRootPost ? '150' : '250'}x>' ${output}`,
                (err) =>
                {
                    if(err) return reject(err);
                    else return resolve();
                }
            );
        }
        catch(err)
        {
            return reject(err);
        }
    });
}