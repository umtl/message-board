const c = require('./captcha.js');

module.exports = (req, res, next) =>
{

    if(!c.captcha_is_correct(req))
    {
        let to_send = { errCode : 900 };
        if(req.body && req.body.change_captcha)
            to_send.svg = c.set_captcha_get_svg(req, req.body.change_captcha);
        return res.status(422).json(to_send);
    }

    return next();
}