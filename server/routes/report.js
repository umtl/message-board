const router = require('express').Router();
const f      = require('../pgFunc.js');
const val    = require('validator');

router.post('/api/createReport/', require('../captchaMW.js'), require('../banBlockMW.js'), (req, res) =>
{
    if(!req.body.id || !val.isNumeric(String(req.body.id)))
        return res.status(400).json({ errCode : 111 });
    if(!req.body.content || !req.body.content.length)
        return res.status(400).json({ errCode : 222 });

    f.createReport
    (
        req.body.id,
        req.body.content,
        (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim()
    )
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(500).json({ errCode : 333 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 444 });
    });
});

module.exports = router;