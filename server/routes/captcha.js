const router = require('express').Router();
const c      = require('../captcha.js');

router.post('/api/captcha', (req, res) =>
{
    return res.json({ svg : c.set_captcha_get_svg(req, true) });
});

module.exports = router;