const router = require('express').Router();
const f      = require('../pgFunc.js');
const rmrf   = require('rimraf');

router.get('/api/' + process.env.modcode + 'reports/', (req, res) =>
{
    f.getReport(req.query.num)
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(400).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.post('/api/' + process.env.modcode + 'resolvereport', (req, res) =>
{
    if(!req.body || !req.body.reportid) return res.status(400).json({ errCode : 1 });
    f.resolveReport(req.body.reportid)
    .then(() =>
    {
        return res.json({ success : true });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.post('/api/' + process.env.modcode + 'banip', (req, res) =>
{
    if(!req.body || !req.body.reason || !req.body.postid) return res.status(400).json({ errCode : 1 });
    f.getPostIP(req.body.postid)
    .then((ip) =>
    {
        return f.setBan(ip, req.body.postid, null, req.body.reason);
        /*
        if(req.body.override)
            return f.setBan(ip, req.body.postid, null, req.body.reason);
        else
            return f.isBanned(ip).then((i) =>
            {
                if(!i) return f.setBan(ip, req.body.postid, null, req.body.reason);
                else throw 4;
            })
            .catch((err) =>
            {
                throw err;
            });
        */
    })
    .then((result) =>
    {
        if(result) return res.json({ success : true });
        else return res.status(400).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        if(err == 4) return res.status(400).json({ errCode : 4 });
        else
        {
            console.error(err);
            return res.status(500).json({ errCode : 2 });
        }
    });
});

router.post('/api/' + process.env.modcode + 'deletpost', (req, res) =>
{
    if(!req.body || !req.body.postid) return res.status(400).json({ errCode : 1 });
    f.deletePost(req.body.postid, req.body.flag || 1)
    .then((result) =>
    {
        rmrf('./public/file/' + req.body.postid + '*', (err) =>
        {
            if(err) console.error('Image delete error', err);
        });

        if(req.body.reportid)
        {
            f.resolveReport(req.body.reportid)
            .then((result) =>
            {
                if(result) return res.json({ success : true });
                else throw result;
            })
            .catch((err) =>
            {
                console.error('report resolve error', err);
                return res.status(500).json({ errCode : 3 });
            });
        }
        else return res.json({ success : true });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
})

router.get('/api/viewban', (req, res) =>
{
    let ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim();

    f.getbanInfo(ip)
    .then((result) =>
    {
        return res.json(result);
    })
    .catch((err) =>
    {
        console.log(err);
        return res.status(500).json({ errCode : 2 });
    });
})

router.get('/api/' + process.env.modcode + 'banlist', (req, res) =>
{
    f.getbanList()
    .then((result) =>
    {
        return res.json(result);
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.post('/api/' + process.env.modcode + 'expireban', (req, res) =>
// TODO make this and others only accessible to mods
{
    if(!req.body || !req.body.postid) return res.status(400).json({ errCode : 1 });

    f.expireBan(req.body.postid)
    .then((result) =>
    {
        return res.json(result);
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

module.exports = router;