const router = require('express').Router();
const f      = require('../pgFunc.js');

router.get('/api/user', (req, res) =>
{
    if(req.session && req.session.uname)
        return res.json({ name : req.session.uname });
    else
        return res.status(401).json({ });
});

router.post('/api/login', (req, res) =>
// TODO: absolutely need to rate limit this, do it from nginx + here if time avail.
{
    if(req.session && req.session.uname)
        return res.status(400).json({ errCode : 3 });

    if
    (
        typeof(req.body.username) !== 'string' ||
        !req.body.username.trim().length       ||
        typeof(req.body.password) !== 'string'
    )
    {
        return res.status(400).json({ errCode : 1 });
    }

    // let ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim();
    // let ua = req.get('user-agent').substr(0, 95);

    f.login_account
    (
        req.body.username.trim().substr(0, 20),
        req.body.password,
    )
    .then((result) =>
    {
        if(result === -2) return res.status(400).json({ errCode : 24 }); //duplicate
        else if(result === -1) return res.status(500).json({ errCode : 22 });
        if(typeof(result) !== 'string') throw 'expected username return';
        req.session.uname = result;
        return res.json({ });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.post('/api/logout', (req, res) =>
{
    if(req.session && req.session.uname)
    {
        delete req.session.uname;
        return res.json({ });
    }
    else
    {
        return res.status(400).json({ errCode : 33 });
    }
});

module.exports = router;