const router = require('express').Router();
const path   = require('path');

router.use(require('./captcha.js'));
router.use(require('./account.js'));
router.use(require('./post.js'));
router.use(require('./userInfo.js'));
router.use(require('./replies.js'));
router.use(require('./report.js'));
router.use(require('./mod.js'));
router.get('/file/:name', (req, res) =>
{
    return res.status('404').sendFile(path.join(__dirname, '../public/file/404.jpg'));
});

router.use('*', (req, res) => {
    console.log('404ed:', req.method, req.originalUrl);
    return res.status(400).json({ info : 'API not defined' });
});

module.exports = router;