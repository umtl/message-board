const router = require('express').Router();
const f      = require('../pgFunc.js');

router.post('/api/countReplies/', (req, res) =>
{
    if(!req.body.id_arr || !Array.isArray(req.body.id_arr))
        return res.status(404).json({ errCode : 1 });
    else if(!req.body.id_arr.length)
        return res.json([]);

    f.countReplies(req.body.id_arr)
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(404).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

module.exports = router;