const router = require('express').Router();
const f      = require('../pgFunc.js');

router.get('/api/userInfo/:user', (req, res) =>
{
    let out = { bio : null, created: null, errCode : null };
    if(!req.params.user || !req.params.user.trim().substr(0, 20).length)
    {
        out.errCode = 611;
        return res.status(404).json(out);
    }

    f.readUserInfo(req.params.user.trim().substr(0, 20))
    .then((result) =>
    {
        if(result)
        {
            out.bio = result.bio;
            out.created = result.created;
            out.errCode = result.errCode;

            return res.json(result);
        }
        else throw result;
    })
    .catch((err) =>
    {
        if(err)
        {
            console.error(err);
            out.errCode = 612;
            return res.status(500).json(out);
        }
        else
        {
            out.errCode = 611;
            return res.status(404).json(out);
        }
    });
});
module.exports = router;