const router = require('express').Router();
const f      = require('../pgFunc.js');
const remove = require('fs-extra').remove;
const thumb  = require('../thumbnail.js');

const cacherMW = require('apicache')
.options({ redisClient : require('../redisFunc.js').rclient })
.middleware;

router.post('/api/createPost', require('../fileUploadMW.js'), require('../captchaMW.js'), require('../banBlockMW.js'), (req, res) =>
{
    let has_pic = req.files && req.files.pic && req.files.pic.tempFilePath;
    let picremover = () => {};
    if(has_pic) picremover = () => remove
    (
        req.files.pic.tempFilePath,
        (err) => { if(err) console.error('async delete err', err); else console.log('deleted', req.files.pic.tempFilePath); }
    );

    if
    (
        has_pic &&
        !(
            req.files.pic.mimetype == 'image/jpeg' ||
            req.files.pic.mimetype == 'image/png'  ||
            req.files.pic.mimetype == 'image/gif'
        )
    )
    {
        picremover();
        return res.status(400).json({ errCode : 33 });
    }

    if(typeof(req.body.content) !== 'string')
    {
        picremover();
        return res.status(400).json({ errCode : 1 });
    }

    req.body.content = req.body.content.trim();

    if(!req.body.content.length)
    {
        picremover();
        return res.status(400).json({ errCode : 1 });
    }

    let ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim();
    f.isBanned(ip)
    .then((isBanned) =>
    {
        console.log('isbanned', isBanned);
        if(isBanned) return res.status(401).json({ errCode : 666, banned : 1 });
        else return f.createPost
        (
            req.body.content,
            req.body.rootPost,
            has_pic ? req.files.pic.name : null,
            req.body.board,
            ip
        )
    })
    .then((id) =>
    {
        if(has_pic)
        {
            return req.files.pic.mv('./public/file/' + id + '_' + req.files.pic.name)
            .then(() => thumb(
                    './public/file/' + id + '_' + req.files.pic.name,
                    './public/file/' + id + '.jpg',
                    req.body.rootPost,
                    req.files.pic.mimetype == 'image/gif'))
            .then(() => res.json({ id : id }));
        }
        else
        {
            return res.json({ id : id });
        }
    })
    .catch((err) =>
    {
        picremover();
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.get('/api/pagepost/:id', cacherMW('5 second'), (req, res) =>
{
    if(!req.params.id.length || req.params.id.length > 20)
        return res.status(404).json({ errCode : 1 });

    f.pagePosts(req.params.id)
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(404).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.get('/api/readPost/:id', (req, res) =>
{
    if(!req.params.id.length || req.params.id.length > 20)
        return res.status(404).json({ errCode : 1 });

    f.readPost(req.params.id)
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(404).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.get('/api/rootPost/:id', (req, res) =>
{
    if(!req.params.id.length || req.params.id.length > 20)
        return res.status(404).json({ errCode : 1 });

    f.rootPost(req.params.id)
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(404).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.get('/api/latestPost/:page', cacherMW('5 second'), (req, res) =>
{
    f.latestPost(req.params.page)
    .then((result) =>
    {
        if(result) return res.json({ posts : result });
        else       return res.status(500).json({ errCode : 1 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

router.get('/api/postByUser/:user', (req, res) =>
{
    if(!req.params.user.length || req.params.user.length > 20)
        return res.status(404).json({ errCode : 1 });

    f.postByUser(req.params.user)
    .then((result) =>
    {
        if(result) return res.json(result);
        else       return res.status(404).json({ errCode : 3 });
    })
    .catch((err) =>
    {
        console.error(err);
        return res.status(500).json({ errCode : 2 });
    });
});

module.exports = router;