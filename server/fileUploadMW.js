const fileupload = require('express-fileupload');
module.exports = fileupload
({
    limits        :
    {
        fileSize  : 50 * 1024 * 1024,
        files     : 1, // number of files
        fields    : 10 // req.body field limit
    },
    useTempFiles  : true,
    tempFileDir   : '/tmp/filebuffer/',
    createParentPath : true,
    safeFileNames : true,
    preserveExtension : 4,
    debug         : false
});