const redis = require('redis');

const rclient = redis.createClient(null, process.env.HOST);

async function rget(key)
{
    return new Promise((resolve, reject) =>
    {
        rclient.get(key, (err, reply) =>
        {
            if(err) return reject(err);
            else    return resolve(reply);
        });
    });
}

module.exports =
{
    rclient : rclient,
    rget    : rget
}