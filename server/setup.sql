-- DROP TABLE post, report, postlog, reportlog, mod, ban, modlog CASCADE;

CREATE TABLE post
(
    id       SERIAL PRIMARY KEY,
    content  TEXT CHECK(LENGTH(content) < 10000),
    fname    TEXT CHECK(LENGTH(fname) < 100),
    replies  SMALLINT DEFAULT 0,
    bumped   TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    rootpost INTEGER,
    board    SMALLINT CHECK(board > 0) DEFAULT 1,
    flag     SMALLINT,
    created  TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE report
(
    id       SERIAL PRIMARY KEY,
    postid   INTEGER NOT NULL,
    resolved BOOLEAN DEFAULT false,
    content  TEXT CHECK(LENGTH(content) < 10000),
    created  TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE postlog
(
    id      INTEGER NOT NULL PRIMARY KEY,
    ip      TEXT CHECK(LENGTH(ip) < 100),
    created TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE reportlog
(
    id      INTEGER NOT NULL PRIMARY KEY,
    ip      TEXT CHECK(LENGTH(ip) < 100),
    created TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE mod
(
    uname TEXT NOT NULL CHECK(LENGTH(uname) < 100) PRIMARY KEY,
    passd TEXT NOT NULL CHECK(LENGTH(uname) < 10000)
);

CREATE TABLE ban
(
    ip      TEXT CHECK(LENGTH(ip) < 100) PRIMARY KEY,
    postid  INTEGER NOT NULL,
    until   TIMESTAMP WITH TIME ZONE DEFAULT NULL, -- null is forever
    created TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE modlog -- for report resolving, post deleting, ban set/removal
(
    theid   INTEGER NOT NULL PRIMARY KEY,
    mod     TEXT REFERENCES mod(uname),
    flag    SMALLINT, -- action taken
    reason  TEXT CHECK(LENGTH(reason) < 10000),
    created TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

-- INSERT INTO mod(uname, passd) VALUES('a', 'a');