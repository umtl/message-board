FROM node:10.15.3-alpine

RUN apk --update add imagemagick && \
    rm -rf /var/cache/apk/*