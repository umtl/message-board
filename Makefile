e:
	code-oss ./server/
	code-oss ./client/
up:
	sudo docker-compose -f dev_docker-compose.yml up -d
	sudo docker-compose -f dev_docker-compose.yml logs -f
down:
	sudo docker-compose -f dev_docker-compose.yml down
d:
	sudo docker-compose -f dev_docker-compose.yml down
logs:
	sudo docker-compose -f dev_docker-compose.yml logs -f
l:
	sudo docker-compose -f dev_docker-compose.yml logs -f
