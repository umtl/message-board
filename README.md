# Dev setup

+ In ./client and ./server `npm install`
+ `make up`
+ cd in ./server and run `make set` to setup db relations
+ Open `http://localhost:9001`

# TODO

+ Add reaction emojis
+ Prevent deleted post from being able to be reported
+ Global donation popup/TTS message